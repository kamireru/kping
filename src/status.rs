use core::fmt;

#[derive(Debug)]
pub struct Status {
    pub state: State,
    pub latency: Latency,
}

impl Status {
    pub fn new() -> Self {
        Self {
            state: State::new(),
            latency: Latency::new(),
        }
    }

}

impl fmt::Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "state=({}), latency=({})", self.state, self.latency)
    }
}


#[derive(Debug)]
pub struct State {
    pub err: u16,
    pub alive: u16,
    pub dead: u16,
}

impl State {
    fn new() -> Self {
        Self { err: 0, alive: 0, dead: 0 }
    }

    fn total(&self) -> u16 {
        self.err + self.alive + self.dead
    }

    fn percent(&self, number: u16) -> f32 {
        (number as f32) / (self.total() as f32 / 100.0)
    }
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f,  "alive={}/{} ({:.0}%)", self.alive, self.total(), self.percent(self.alive))?;
        write!(f, ", dead={}/{} ({:.0}%)", self.dead, self.total(),  self.percent(self.dead))?;
        write!(f,  ", err={}/{} ({:.0}%)", self.err, self.total(),   self.percent(self.err))
    }
}


#[derive(Debug)]
pub struct Latency {
    min_ms: f64,
    max_ms: f64,
    avg_ms: f64,
}

impl Latency {
    pub fn new() -> Self {
        Self {
            min_ms: std::f64::NAN,
            max_ms: std::f64::NAN,
            avg_ms: std::f64::NAN,
        }
    }

    pub fn update_range(&mut self, latency: f64) {
        if self.min_ms.is_nan() || latency < self.min_ms  {
            self.min_ms = latency
        }
        if self.max_ms.is_nan() ||  self.max_ms < latency {
            self.max_ms = latency
        }
    }

    pub fn update_avg(&mut self, latency: f64) {
        self.avg_ms = latency;
    }

    fn precision(&self, number: f64) -> usize {
        match number {
            n if n < 0.1  => 3,
            n if n < 0.0  => 2,
            _             => 1,
        }
    }
}

impl fmt::Display for Latency {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f,   "min={:.*} ms", self.precision(self.min_ms), self.min_ms)?;
        write!(f, ", avg={:.*} ms", self.precision(self.avg_ms), self.avg_ms)?;
        write!(f, ", max={:.*} ms", self.precision(self.max_ms), self.max_ms)
    }
}
