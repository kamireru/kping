use clap::App;
use clap::Arg;

use std::error::Error;
use std::fs;
use std::io;
use std::io::BufRead;
use std::slice::Iter;

use std::time;

#[derive(Debug)]
pub struct Config {
    files: Vec<String>,
    addrs: Vec<String>,

    interval: time::Duration,
    timeout:  time::Duration,
    count   : u16,
}

impl Config {
    pub fn parse() -> Result<Self, Box<dyn Error>> {
	let matches = App::new(crate_name!())
	    .version(crate_version!())
	    .about(crate_description!())
	    .arg(Arg::with_name("files")
		 .help("path to file with targets to ping (one target per line)")
		 .short("f")
		 .long("file")
		 .takes_value(true)
		 .multiple(true)
		 .number_of_values(1))
	    .arg(Arg::with_name("addrs")
                 .multiple(true)
                 .number_of_values(1))
            .arg(Arg::with_name("count")
                 .help("number of ping cycles (default unlimited)")
                 .short("c")
                 .long("count")
		 .takes_value(true))
            .arg(Arg::with_name("interval")
                 .help("interval between ping cycles (in seconds, default 5)")
                 .short("i")
                 .long("interval")
		 .takes_value(true))
            .arg(Arg::with_name("timeout")
                 .help("ping timeout (default 80% of interval)")
                 .short("t")
                 .long("timeout")
		 .takes_value(true))
	    .get_matches();


        let files = match matches.values_of("files") {
            None         => Vec::new(),
            Some(values) => values.map(|v| v.to_string()).collect(),
        };

        let addrs = match matches.values_of("addrs") {
            None         => Vec::new(),
            Some(values) => values.map(|v| v.to_string()).collect(),
        };

        let interval = match matches.value_of("interval") {
            None        => time::Duration::from_secs(5),
            Some(value) => {
                let millis = (value.parse::<f32>()? * 1000.0).ceil() as u64;
                time::Duration::from_millis(millis)
            }
        };

        let timeout = match matches.value_of("timeout") {
            None        => time::Duration::from_millis(((interval.as_millis() / 5) * 4) as u64),
            Some(value) => {
                let millis = (value.parse::<f32>()? * 1000.0).ceil() as u64;
                time::Duration::from_millis(millis)
            }
        };

        let count = match matches.value_of("count") {
            None        => std::u16::MAX,
            Some(value) => value.parse::<u16>()?,
        };

        Ok(Self { files, addrs, interval, timeout, count })
    }

    /// Parse given file and return array of target addrs stored in it.
    pub fn parse_file<S: Into<String>>(path: S) -> Result<Vec<String>, io::Error> {
        let file = fs::File::open(path.into())?;
        let read = io::BufReader::new(&file);

        let mut result = Vec::new();
        for line in read.lines() {
            result.push(line?)
        }

        Ok(result)
    }

    /// Return iterator to files stored in config structure
    pub fn files_iter(&self) -> Iter<String> {
        self.files.iter()
    }

    /// Return interval in which to repeat ping
    pub fn interval(&self) -> &time::Duration {
        &self.interval
    }

    /// Return timeout in which to repeat ping
    pub fn timeout(&self) -> &time::Duration {
        &self.timeout
    }

    /// Return count of ping attemts to repeat (use -1 for infinty)
    pub fn count(&self) -> u16 {
        self.count
    }

    /// Return iterator to addresses stored in config structure
    pub fn addrs_iter(&self) -> Iter<String> {
        self.addrs.iter()
    }
}
