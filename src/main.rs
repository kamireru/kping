#[macro_use] extern crate log;

use kping::Config;
use kping::KiloPing;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::Builder::from_default_env()
        .default_format_timestamp(true)
        .default_format_timestamp_nanos(false)
        .default_format_module_path(false)
        .default_format_level(true)
        .filter_level(log::LevelFilter::Info)
        .init();

    let config = Config::parse()?;
    let kping  = KiloPing::from_config(&config);

    kping.ping_loop(config.interval(), config.count())?;

    Ok(())
}
