#[macro_use] extern crate clap;
#[macro_use] extern crate log;

use oping::Ping;
use oping::PingResult;

use std::time;
use std::thread;

pub mod config;
pub mod status;

pub use config::Config;
pub use status::Status;


#[derive(Debug)]
pub struct KiloPing {
    addrs: Vec<String>,

    // oping is using f64 seconds for timeout
    timeout: f64,
}

impl KiloPing {

    pub fn new(addrs: Vec<String>, timeout: &time::Duration) -> Self {
        let timeout = (timeout.as_secs() as f64) + (0.001f64 * timeout.subsec_millis() as f64);
        Self { addrs, timeout: timeout }
    }

    pub fn from_config(config: &Config) -> Self {
        let mut result = Self::new(vec![], config.timeout());

        for addr in config.addrs_iter() {
            result.add_addr(addr)
        }

        for file in config.files_iter() {
            match Config::parse_file(file) {
                Err(error) => {
                    error!("File '{}': {}", file, error);
                },
                Ok(addrs)  => addrs.iter().map(|addr| result.add_addr(addr)).collect(),
            }
        }

        result
    }

    pub fn add_addr<S: Into<String>>(&mut self, addr: S) {
        self.addrs.push(addr.into());
    }

    pub fn ping(&self) -> PingResult<Status> {
        let mut status = Status::new();
        let mut latency_sum: f64 = 0.0;

        let mut oping  = Ping::new();
        oping.set_timeout(self.timeout)?;

        for addr in self.addrs.iter() {
            if let Err(error) = oping.add_host(addr.as_str()) {
                debug!("Target '{}': {}", addr, error);
                status.state.err += 1;
            }
        }

        for response in oping.send()? {
            if 0 < response.dropped {
                status.state.dead += 1;
            }
            else {
                status.state.alive += 1;
                status.latency.update_range(response.latency_ms);

                // accumulate latency of alive hosts for average computation
                latency_sum += response.latency_ms;
            }
        }

        status.latency.update_avg(latency_sum / (status.state.alive as f64));

        Ok(status)
    }

    pub fn ping_loop(&self, interval: &time::Duration, count: u16) -> PingResult<()> {
        let mut now;
        let mut count:i32 = count as i32;
        while 0 < count {
            count -= 1;
            now    = time::Instant::now();

            info!("{}", self.ping()?);

            if now.elapsed() < *interval {
                thread::sleep(*interval - now.elapsed());
            }
        }

        Ok(())
    }
}
