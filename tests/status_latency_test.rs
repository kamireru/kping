extern crate kping;

use kping::status::Latency;

#[test]
fn test_display_with_new_instance() {
    assert_eq!("min=NaN ms, avg=NaN ms, max=NaN ms", format!("{}", Latency::new()));
}

#[test]
fn test_display_with_min_max() {
    let mut latency = Latency::new();
    latency.update_range(0.1);
    latency.update_range(0.2);

    assert_eq!("min=0.1 ms, avg=NaN ms, max=0.2 ms", format!("{}", latency));
}

#[test]
fn test_display_with_avg() {
    let mut latency = Latency::new();
    latency.update_avg(0.1);

    assert_eq!("min=NaN ms, avg=0.1 ms, max=NaN ms", format!("{}", latency));
}
