README
======

[![pipeline status](https://gitlab.com/kamireru/kping/badges/master/pipeline.svg)](https://gitlab.com/kamireru/kping/commits/master)

## Overview

The `kping` project uses `oping` library to repeatedly ping many (thousands)
hosts, displaying single summary line for all of them.

Unlike `fping`, `oping`, `ping` utils, it is not concerted about which host
is alive and which not. Just number of live/dead hosts.

## Requirements

Functional `Rust` installation

* https://www.rust-lang.org/tools/install


## Build and installation

Following command will build and install `kping` binary into `$HOME/bin`:

	$ cargo install --path . --root $HOME

On some systems, it is necessary to set additional capabilities to the binary,
to allow it access to the socket(s) when run as non-root user:

	$ sudo getcap cap_net_raw=ep $HOME/bin/kping


## Example usage

	$ for I in `seq 1 254`; do echo "10.44.26.$I" >> targets; done
	$ sudo ~/bin/kping -f targets -i 5
	state=(alive=197/254 (78%), dead=57/254 (22%), err=0/254 (0%)), latency=(min=0.019 ms, avg=0.3 ms, max=2.5 ms)
	state=(alive=196/254 (77%), dead=58/254 (23%), err=0/254 (0%)), latency=(min=0.026 ms, avg=0.7 ms, max=13.2 ms)
	state=(alive=197/254 (78%), dead=57/254 (22%), err=0/254 (0%)), latency=(min=0.037 ms, avg=0.7 ms, max=17.7 ms)
	state=(alive=197/254 (78%), dead=57/254 (22%), err=0/254 (0%)), latency=(min=0.022 ms, avg=0.8 ms, max=30.2 ms)
	state=(alive=198/254 (78%), dead=56/254 (22%), err=0/254 (0%)), latency=(min=0.046 ms, avg=0.8 ms, max=15.8 ms)
